# OpenML dataset: ames_housing

https://www.openml.org/d/43926

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Predict sales prices of houses. The Ames Housing dataset was compiled by Dean De Cock for use in data science education.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43926) of an [OpenML dataset](https://www.openml.org/d/43926). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43926/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43926/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43926/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

